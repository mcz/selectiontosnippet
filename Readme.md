# Selection To Snippet


## description
file content to vscode snippet formatted.    
Take a snippet file (ex: **defaultComp.vue**) as input ask for the title & description    
then generate a **Snippet.txt** file.    
You can now copy past the content to your vscode snippet

## How to use
````python
python3 selectionToSnippet.py
````

Example of output 
````txt
"Print to console": {
    "scope": "vue"
    "prefix": "Vue3 start",
    "description": "Simple vue3 starting component"
    "body": [
      "<template>",
      "  <div class="main-css">",
      "    <p>result: {{ myNumber }}</p>",
      "    <button @click="increment">Add</button>",
      "  </div>",
      "</template>",
    ],
}

````
